Veritas
=======

Veritas standalone version is an open source tool TrueBeam Developer Mode
provided by Varian Medical Systems, Palo Alt. Veritas makes the TrueBeam
Developer Mode more accessible by reducing the
learning curve, and quickly reaching a correct and intended delivery pattern.
As an open source initiative, Veritas also provides a platform for free flow
of ideas among researchers.  A user may start from an anonymized DICOM file or
previous XML example and introduce delivery modifications, or begin their
experiment from scratch. Veritas is written using exclusively open source
Python code and tools, so expert users may download the source code and extend
functionality for their own purposes.

For questions, please send us an email at: TrueBeamDeveloper@varian.com


Main functionalities in version 2.2 are as follows
==================================================
* **Create** a new research beam.
* **Modify** an existing research beam
* **Convert** DICOM-RT to an XML.
* **Insert imaging points** (kV and MV) in an research beam.
* **Convert scale** between IEC 61217 and Varian scale or IEC 60601-2-1.
* **Error check** before final research beam is generated.

News
====
5/11/2015

Version 2.2 released.

Latest Features
===============
* Imaging points now are saved automatically upon navigation
* Now user can create a mock TrueBeam point from scratch on the imaging modal
* Automatically ensures that a user defines both start and stop points for continuous imaging
* User can now plot any two XML elements against each other, instead of merely MU and


Bug Fixes
=========
5/11/2015

* MV imaging points no longer produces buggy and invalid XML schema
* Previous button on imaging modal now works

10/24/2014

* Added a factor of 10 to convert MLC values from mm to cm.

Project Details
===============

:Code:            https://bitbucket.org/varianveritas/veritas/
:Issue tracker:   https://bitbucket.org/varianveritas/veritas/issues/
:Documentation:   http://varianveritas.readthedocs.org/en/latest/
:License:         Varian Open Source 1.0

Warning
=======
**Developer Mode is intended for non-clinical use only and is NOT cleared for use on humans**
