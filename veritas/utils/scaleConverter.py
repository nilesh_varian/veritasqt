from xml.etree import ElementTree as ET
import numpy as np

class ScaleConverter:
    '''
    Convert IEC616217 scale (from DICOM) to Varian Scale or IEC6060121 
    '''
    def __init__(self, filename):
        
        self.filename = filename 
        self.openFile(filename)
                
        self.IEC61217_to_VarianScale = {
                        'X1':           lambda x: -x,
                        'X2':           lambda x: x,
                        'Y1' :          lambda x: -x,
                        'Y2':           lambda x: x,
                        'GantryRtn':    lambda x: (180 - x) % 360,
                        'CollRtn':      lambda x: (180 - x) % 360,
                        'CouchRtn':     lambda x: (180 - x) % 360,                       
                        'CouchLat':     lambda x: (100 + x),
                        'CouchVrt':     lambda x: (100 - x) % 200,
                        }
        
        self.IEC61217_to_IEC6060121 = {
                    'X1':           lambda x: -x,
                    'X2':           lambda x: -x,
                    'Y1' :          lambda x: -x,
                    'Y2':           lambda x: -x,                                       
                    'CouchLat':     lambda x: (1000 + x) % 1000,                    
                    'CouchVrt':     lambda x: (1000 - x) % 1000,
                }
                
    def openFile(self, filename):
        '''
        Open the given xml file and 
        extract its root element
        '''        
        with open(filename, 'r') as f:
            self.tree = ET.parse(f)
            self.root = self.tree.getroot()            
            self.sbeam = self.root.getchildren()[0]
                        
    def convertScale(self, scaleType):
        '''
        Convert IEC61217 scale to Varian scale or IEC6060121 depending on scaleType
        '''
        # Traverse through the list of different axes 
        # and do scale conversion based on functions 
        # listed in self.conversion field   
        if(scaleType == 'VarianScale'):       
            for axis in self.IEC61217_to_VarianScale.keys():
                for lst in self.root.iter(axis):
                    if lst.text is not None:
                        newValue = self.IEC61217_to_VarianScale[lst.tag](float(lst.text))
        elif(scaleType == 'IEC6060121'):
            for axis in self.IEC61217_to_IEC6060121.keys():
                for lst in self.root.iter(axis):
                    if lst.text is not None:
                        newValue = self.IEC61217_to_IEC6060121[lst.tag](float(lst.text))
                    
                lst.text = str(newValue)
        
        # In the end, change the sign of 'B' mlc leaves
        for lst in self.root.iter('B'):
            newValue  = -np.array(lst.text.split(), dtype='float') # sign conversion
            #twoDecimals = ["%.2f" % v for v in newValue]           # Two decimal places
            #lst.text = str(twoDecimals).replace('\n', '').replace("'", "").replace(",","")[1:-1]
            lst.text = str(newValue).replace('\n', '').replace("'", "").replace(",","")[1:-1]

