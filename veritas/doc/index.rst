.. Varian Veritas documentation master file, created by
   sphinx-quickstart on Tue Apr 22 20:22:31 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome
=======

Welcome to Varian Veritas version 1.0, an open source tool to facilitate the creation as well as
modification of an XML beam for TrueBeam Developer Mode. A graphical user interface guides a user through a
series of steps for creating a new XML beam or modifying an exiting XML beam. A user may start from an
anonymized DICOM file or previous XML example and introduce delivery modifications, or begin their
experiment from scratch. Veritas is written using exclusively open source Python code and tools, so expert
users may download the source code and extend functionality for their own purposes.

If you are seeing this project for the first time and want to use it to generate xml, you are advised to read the :doc:`installation` and :doc:`tutorial` section.

If you wish to contribute, reading the :doc:`preface` section gives an overview of the overall structure
and documentation gives a rudimentary overview of what important functions / methods

**Contents**

.. toctree::
   :maxdepth: 2

   installation
   tutorial
   preface
   documentation
   more_info
   citing
   license

.. warning::

   **Developer Mode is intended for non-clinical use only and is NOT cleared for use on humans.**
