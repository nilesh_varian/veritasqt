More info
---------

**Need Help?**

Please check the faq, the :doc:`api docs <documentation>`. For general Python related questions
please refer to `stackoverflow <http://stackoverflow.com/>`_.

You can file software bugs, patches and feature requests on the bitbucket `tracker <https://bitbucket.org/varianveritas/veritas/issues/>`_ .

To keep up to date with what's going on in Varian Veritas, see the what's new page or browse the `source code <https://bitbucket.org/varianveritas/veritas/>`_

or

::

   git clone https://bitbucket.org/varianveritas/veritas.git
