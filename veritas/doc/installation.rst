Installation
============

.. toctree::
   :maxdepth: 4

Requirements
------------

The list of dependencies are as follows:

* `Python 2.7.x <http://www.python.org/>`_
* `PySide 1.2.2 <https://pypi.python.org/pypi/PySide/1.2.0>`_
* `pydicom 0.9.8 <https://pypi.python.org/pypi/pydicom/>`_
* `NumPy 1.8.1 <http://www.scipy.org/scipylib/download.html>`_
* `matplotlib 1.4.3 <http://matplotlib.org/>`_

These libraries can installed either through `easy_install <https://pypi.python.org/pypi/setuptools>`_

.. 
   easy_install SomePackage

or `pip <https://pypi.python.org/pypi/pip>`_

..
   pip install SomePackage

Tests
-----

If you wish to run the tests, you would also need to install the `nose package <https://nose.readthedocs.org/en/latest/>`_.
To run the tests, simply type

   ``nosetests``


Supported platform
------------------

* Microsoft Windows 7
* GNU/Linux - ubuntu 12.04 LTS

