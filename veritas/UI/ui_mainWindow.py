# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'UI_Qt/mainWindow.ui'
#
# Created: Tue Apr 21 13:11:25 2015
#      by: pyside-uic 0.2.15 running on PySide 1.2.2
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.setEnabled(True)
        MainWindow.resize(943, 700)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(MainWindow.sizePolicy().hasHeightForWidth())
        MainWindow.setSizePolicy(sizePolicy)
        MainWindow.setMinimumSize(QtCore.QSize(943, 675))
        MainWindow.setMaximumSize(QtCore.QSize(943, 700))
        font = QtGui.QFont()
        font.setPointSize(9)
        MainWindow.setFont(font)
        MainWindow.setMouseTracking(True)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("images/truebeam2.jpg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        MainWindow.setWindowIcon(icon)
        MainWindow.setWindowOpacity(1.0)
        MainWindow.setStatusTip("")
        MainWindow.setAutoFillBackground(False)
        MainWindow.setStyleSheet("")
        MainWindow.setIconSize(QtCore.QSize(24, 24))
        MainWindow.setToolButtonStyle(QtCore.Qt.ToolButtonIconOnly)
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.beamonButton = QtGui.QPushButton(self.centralwidget)
        self.beamonButton.setGeometry(QtCore.QRect(50, 20, 111, 51))
        font = QtGui.QFont()
        font.setPointSize(11)
        self.beamonButton.setFont(font)
        self.beamonButton.setToolTip("")
        self.beamonButton.setStatusTip("")
        self.beamonButton.setObjectName("beamonButton")
        self.imagingButton = QtGui.QPushButton(self.centralwidget)
        self.imagingButton.setGeometry(QtCore.QRect(260, 20, 111, 51))
        font = QtGui.QFont()
        font.setPointSize(11)
        self.imagingButton.setFont(font)
        self.imagingButton.setToolTip("")
        self.imagingButton.setStatusTip("")
        self.imagingButton.setObjectName("imagingButton")
        self.textBrowser = QtGui.QTextBrowser(self.centralwidget)
        self.textBrowser.setGeometry(QtCore.QRect(30, 150, 481, 371))
        self.textBrowser.setAutoFillBackground(False)
        self.textBrowser.setStyleSheet("")
        self.textBrowser.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)
        self.textBrowser.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)
        self.textBrowser.setObjectName("textBrowser")
        self.label = QtGui.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(30, 110, 141, 21))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.xmlTextEdit = QtGui.QPushButton(self.centralwidget)
        self.xmlTextEdit.setGeometry(QtCore.QRect(390, 530, 91, 31))
        self.xmlTextEdit.setObjectName("xmlTextEdit")
        self.plotAxes = QtGui.QPushButton(self.centralwidget)
        self.plotAxes.setGeometry(QtCore.QRect(670, 460, 91, 31))
        self.plotAxes.setObjectName("plotAxes")
        self.warning_label = QtGui.QLabel(self.centralwidget)
        self.warning_label.setGeometry(QtCore.QRect(20, 570, 741, 40))
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setWeight(75)
        font.setBold(True)
        self.warning_label.setFont(font)
        self.warning_label.setObjectName("warning_label")
        self.imagingTypeBox = QtGui.QComboBox(self.centralwidget)
        self.imagingTypeBox.setGeometry(QtCore.QRect(250, 80, 131, 22))
        self.imagingTypeBox.setObjectName("imagingTypeBox")
        self.imagingTypeBox.addItem("")
        self.imagingTypeBox.addItem("")
        self.xAxisBox = QtGui.QComboBox(self.centralwidget)
        self.xAxisBox.setGeometry(QtCore.QRect(610, 400, 81, 22))
        self.xAxisBox.setObjectName("xAxisBox")
        self.xAxisBox.addItem("")
        self.xAxisBox.addItem("")
        self.xAxisBox.addItem("")
        self.xAxisBox.addItem("")
        self.xAxisBox.addItem("")
        self.xAxisBox.addItem("")
        self.xAxisBox.addItem("")
        self.xAxisBox.addItem("")
        self.xAxisBox.addItem("")
        self.xAxisBox.addItem("")
        self.xAxisBox.addItem("")
        self.xAxisBox.addItem("")
        self.xAxisBox.addItem("")
        self.xAxisBox.addItem("")
        self.yAxisBox = QtGui.QComboBox(self.centralwidget)
        self.yAxisBox.setGeometry(QtCore.QRect(740, 400, 81, 22))
        self.yAxisBox.setObjectName("yAxisBox")
        self.yAxisBox.addItem("")
        self.yAxisBox.addItem("")
        self.yAxisBox.addItem("")
        self.yAxisBox.addItem("")
        self.yAxisBox.addItem("")
        self.yAxisBox.addItem("")
        self.yAxisBox.addItem("")
        self.yAxisBox.addItem("")
        self.yAxisBox.addItem("")
        self.yAxisBox.addItem("")
        self.yAxisBox.addItem("")
        self.yAxisBox.addItem("")
        self.yAxisBox.addItem("")
        self.yAxisBox.addItem("")
        self.verticalLayoutWidget = QtGui.QWidget(self.centralwidget)
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(550, 40, 361, 331))
        self.verticalLayoutWidget.setObjectName("verticalLayoutWidget")
        self.vbox = QtGui.QVBoxLayout(self.verticalLayoutWidget)
        self.vbox.setContentsMargins(0, 0, 0, 0)
        self.vbox.setObjectName("vbox")
        self.mlcLeafX = QtGui.QLineEdit(self.centralwidget)
        self.mlcLeafX.setEnabled(True)
        self.mlcLeafX.setGeometry(QtCore.QRect(620, 460, 31, 31))
        self.mlcLeafX.setObjectName("mlcLeafX")
        self.leafXLabel = QtGui.QLabel(self.centralwidget)
        self.leafXLabel.setGeometry(QtCore.QRect(600, 430, 61, 21))
        self.leafXLabel.setObjectName("leafXLabel")
        self.mlcLeafY = QtGui.QLineEdit(self.centralwidget)
        self.mlcLeafY.setGeometry(QtCore.QRect(780, 460, 31, 31))
        self.mlcLeafY.setObjectName("mlcLeafY")
        self.leafYLabel = QtGui.QLabel(self.centralwidget)
        self.leafYLabel.setGeometry(QtCore.QRect(770, 430, 61, 21))
        self.leafYLabel.setObjectName("leafYLabel")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtGui.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 943, 21))
        self.menubar.setObjectName("menubar")
        self.menuFile = QtGui.QMenu(self.menubar)
        font = QtGui.QFont()
        font.setFamily("Segoe UI")
        self.menuFile.setFont(font)
        self.menuFile.setObjectName("menuFile")
        self.menuHelp = QtGui.QMenu(self.menubar)
        self.menuHelp.setObjectName("menuHelp")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtGui.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.toolBar = QtGui.QToolBar(MainWindow)
        self.toolBar.setMouseTracking(True)
        self.toolBar.setContextMenuPolicy(QtCore.Qt.NoContextMenu)
        self.toolBar.setMovable(False)
        self.toolBar.setToolButtonStyle(QtCore.Qt.ToolButtonTextUnderIcon)
        self.toolBar.setObjectName("toolBar")
        MainWindow.addToolBar(QtCore.Qt.TopToolBarArea, self.toolBar)
        self.actionCreatePlan = QtGui.QAction(MainWindow)
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap("images/filenew.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.actionCreatePlan.setIcon(icon1)
        self.actionCreatePlan.setObjectName("actionCreatePlan")
        self.actionOpenPlan = QtGui.QAction(MainWindow)
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap("images/fileopen.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.actionOpenPlan.setIcon(icon2)
        self.actionOpenPlan.setObjectName("actionOpenPlan")
        self.actionExit = QtGui.QAction(MainWindow)
        icon3 = QtGui.QIcon()
        icon3.addPixmap(QtGui.QPixmap("images/filequit.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.actionExit.setIcon(icon3)
        self.actionExit.setObjectName("actionExit")
        self.actionAbout = QtGui.QAction(MainWindow)
        self.actionAbout.setObjectName("actionAbout")
        self.actionDocuments = QtGui.QAction(MainWindow)
        self.actionDocuments.setObjectName("actionDocuments")
        self.actionSave = QtGui.QAction(MainWindow)
        icon4 = QtGui.QIcon()
        icon4.addPixmap(QtGui.QPixmap("images/filesave.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.actionSave.setIcon(icon4)
        self.actionSave.setObjectName("actionSave")
        self.actionSaveAs = QtGui.QAction(MainWindow)
        icon5 = QtGui.QIcon()
        icon5.addPixmap(QtGui.QPixmap("images/filesaveas.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.actionSaveAs.setIcon(icon5)
        self.actionSaveAs.setObjectName("actionSaveAs")
        self.actionDcm2xml = QtGui.QAction(MainWindow)
        self.actionDicomTree = QtGui.QAction(MainWindow)
        icon6 = QtGui.QIcon()
        icon6.addPixmap(QtGui.QPixmap(":/dicomLogo.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.actionDcm2xml.setIcon(icon6)
        self.actionDcm2xml.setObjectName("actionDcm2xml")
        icon7 = QtGui.QIcon()
        icon7.addPixmap(QtGui.QPixmap("images/tree.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.actionDicomTree.setIcon(icon7)
        self.actionDicomTree.setObjectName("actionDicomTree")
        self.actionLicense = QtGui.QAction(MainWindow)
        self.actionLicense.setObjectName("actionLicense")
        self.menuFile.addAction(self.actionCreatePlan)
        self.menuFile.addAction(self.actionOpenPlan)
        self.menuFile.addAction(self.actionDcm2xml)
        self.menuFile.addAction(self.actionDicomTree)
        self.menuFile.addAction(self.actionSave)
        self.menuFile.addAction(self.actionSaveAs)
        self.menuFile.addAction(self.actionExit)
        self.menuFile.addSeparator()
        self.menuHelp.addAction(self.actionAbout)
        self.menuHelp.addAction(self.actionDocuments)
        self.menuHelp.addAction(self.actionLicense)
        self.menubar.addAction(self.menuFile.menuAction())
        self.menubar.addAction(self.menuHelp.menuAction())
        self.toolBar.addAction(self.actionCreatePlan)
        self.toolBar.addAction(self.actionOpenPlan)
        self.toolBar.addAction(self.actionDcm2xml)
        self.toolBar.addAction(self.actionDicomTree)
        self.toolBar.addAction(self.actionSave)
        self.toolBar.addSeparator()
        self.toolBar.addAction(self.actionExit)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QtGui.QApplication.translate("MainWindow", "MainWindow", None, QtGui.QApplication.UnicodeUTF8))
        self.beamonButton.setText(QtGui.QApplication.translate("MainWindow", "MV beam on", None, QtGui.QApplication.UnicodeUTF8))
        self.imagingButton.setText(QtGui.QApplication.translate("MainWindow", "Imaging", None, QtGui.QApplication.UnicodeUTF8))
        self.label.setText(QtGui.QApplication.translate("MainWindow", "Generated XML File", None, QtGui.QApplication.UnicodeUTF8))
        self.xmlTextEdit.setText(QtGui.QApplication.translate("MainWindow", "Open XML File", None, QtGui.QApplication.UnicodeUTF8))
        self.plotAxes.setText(QtGui.QApplication.translate("MainWindow", "Plot Axes", None, QtGui.QApplication.UnicodeUTF8))
        self.warning_label.setText(QtGui.QApplication.translate("MainWindow", "Developer Mode is intended for non-clinical use only and is NOT cleared for use on humans", None, QtGui.QApplication.UnicodeUTF8))
        self.imagingTypeBox.setItemText(0, QtGui.QApplication.translate("MainWindow", "Inside Treatment", None, QtGui.QApplication.UnicodeUTF8))
        self.imagingTypeBox.setItemText(1, QtGui.QApplication.translate("MainWindow", "Outside Treatment", None, QtGui.QApplication.UnicodeUTF8))
        self.xAxisBox.setItemText(0, QtGui.QApplication.translate("MainWindow", "X-axis", None, QtGui.QApplication.UnicodeUTF8))
        self.xAxisBox.setItemText(1, QtGui.QApplication.translate("MainWindow", "Mu", None, QtGui.QApplication.UnicodeUTF8))
        self.xAxisBox.setItemText(2, QtGui.QApplication.translate("MainWindow", "GantryRtn", None, QtGui.QApplication.UnicodeUTF8))
        self.xAxisBox.setItemText(3, QtGui.QApplication.translate("MainWindow", "CollRtn", None, QtGui.QApplication.UnicodeUTF8))
        self.xAxisBox.setItemText(4, QtGui.QApplication.translate("MainWindow", "CouchVrt", None, QtGui.QApplication.UnicodeUTF8))
        self.xAxisBox.setItemText(5, QtGui.QApplication.translate("MainWindow", "CouchLat", None, QtGui.QApplication.UnicodeUTF8))
        self.xAxisBox.setItemText(6, QtGui.QApplication.translate("MainWindow", "CouchLng", None, QtGui.QApplication.UnicodeUTF8))
        self.xAxisBox.setItemText(7, QtGui.QApplication.translate("MainWindow", "CouchRtn", None, QtGui.QApplication.UnicodeUTF8))
        self.xAxisBox.setItemText(8, QtGui.QApplication.translate("MainWindow", "X1", None, QtGui.QApplication.UnicodeUTF8))
        self.xAxisBox.setItemText(9, QtGui.QApplication.translate("MainWindow", "X2", None, QtGui.QApplication.UnicodeUTF8))
        self.xAxisBox.setItemText(10, QtGui.QApplication.translate("MainWindow", "Y1", None, QtGui.QApplication.UnicodeUTF8))
        self.xAxisBox.setItemText(11, QtGui.QApplication.translate("MainWindow", "Y2", None, QtGui.QApplication.UnicodeUTF8))
        self.xAxisBox.setItemText(12, QtGui.QApplication.translate("MainWindow", "Mlc A", None, QtGui.QApplication.UnicodeUTF8))
        self.xAxisBox.setItemText(13, QtGui.QApplication.translate("MainWindow", "Mlc B", None, QtGui.QApplication.UnicodeUTF8))
        self.yAxisBox.setItemText(0, QtGui.QApplication.translate("MainWindow", "Y-axis", None, QtGui.QApplication.UnicodeUTF8))
        self.yAxisBox.setItemText(1, QtGui.QApplication.translate("MainWindow", "Mu", None, QtGui.QApplication.UnicodeUTF8))
        self.yAxisBox.setItemText(2, QtGui.QApplication.translate("MainWindow", "GantryRtn", None, QtGui.QApplication.UnicodeUTF8))
        self.yAxisBox.setItemText(3, QtGui.QApplication.translate("MainWindow", "CollRtn", None, QtGui.QApplication.UnicodeUTF8))
        self.yAxisBox.setItemText(4, QtGui.QApplication.translate("MainWindow", "CouchVrt", None, QtGui.QApplication.UnicodeUTF8))
        self.yAxisBox.setItemText(5, QtGui.QApplication.translate("MainWindow", "CouchLat", None, QtGui.QApplication.UnicodeUTF8))
        self.yAxisBox.setItemText(6, QtGui.QApplication.translate("MainWindow", "CouchLng", None, QtGui.QApplication.UnicodeUTF8))
        self.yAxisBox.setItemText(7, QtGui.QApplication.translate("MainWindow", "CouchRtn", None, QtGui.QApplication.UnicodeUTF8))
        self.yAxisBox.setItemText(8, QtGui.QApplication.translate("MainWindow", "X1", None, QtGui.QApplication.UnicodeUTF8))
        self.yAxisBox.setItemText(9, QtGui.QApplication.translate("MainWindow", "X2", None, QtGui.QApplication.UnicodeUTF8))
        self.yAxisBox.setItemText(10, QtGui.QApplication.translate("MainWindow", "Y1", None, QtGui.QApplication.UnicodeUTF8))
        self.yAxisBox.setItemText(11, QtGui.QApplication.translate("MainWindow", "Y2", None, QtGui.QApplication.UnicodeUTF8))
        self.yAxisBox.setItemText(12, QtGui.QApplication.translate("MainWindow", "Mlc A", None, QtGui.QApplication.UnicodeUTF8))
        self.yAxisBox.setItemText(13, QtGui.QApplication.translate("MainWindow", "Mlc B", None, QtGui.QApplication.UnicodeUTF8))
        self.leafXLabel.setText(QtGui.QApplication.translate("MainWindow", "MLC (1-60)", None, QtGui.QApplication.UnicodeUTF8))
        self.leafYLabel.setText(QtGui.QApplication.translate("MainWindow", "MLC (1-60)", None, QtGui.QApplication.UnicodeUTF8))
        self.menuFile.setTitle(QtGui.QApplication.translate("MainWindow", "File", None, QtGui.QApplication.UnicodeUTF8))
        self.menuHelp.setTitle(QtGui.QApplication.translate("MainWindow", "Help", None, QtGui.QApplication.UnicodeUTF8))
        self.toolBar.setWindowTitle(QtGui.QApplication.translate("MainWindow", "toolBar", None, QtGui.QApplication.UnicodeUTF8))
        self.actionCreatePlan.setText(QtGui.QApplication.translate("MainWindow", "New Plan", None, QtGui.QApplication.UnicodeUTF8))
        self.actionCreatePlan.setIconText(QtGui.QApplication.translate("MainWindow", "New", None, QtGui.QApplication.UnicodeUTF8))
        self.actionCreatePlan.setToolTip(QtGui.QApplication.translate("MainWindow", "Click to create a new xml ", None, QtGui.QApplication.UnicodeUTF8))
        self.actionCreatePlan.setStatusTip(QtGui.QApplication.translate("MainWindow", "Create a new XML file", None, QtGui.QApplication.UnicodeUTF8))
        self.actionCreatePlan.setShortcut(QtGui.QApplication.translate("MainWindow", "Ctrl+N", None, QtGui.QApplication.UnicodeUTF8))
        self.actionOpenPlan.setText(QtGui.QApplication.translate("MainWindow", "Open Plan", None, QtGui.QApplication.UnicodeUTF8))
        self.actionOpenPlan.setIconText(QtGui.QApplication.translate("MainWindow", "Open", None, QtGui.QApplication.UnicodeUTF8))
        self.actionOpenPlan.setToolTip(QtGui.QApplication.translate("MainWindow", "Click to open existing XML plan", None, QtGui.QApplication.UnicodeUTF8))
        self.actionOpenPlan.setStatusTip(QtGui.QApplication.translate("MainWindow", "Click to open an existing XML plan", None, QtGui.QApplication.UnicodeUTF8))
        self.actionOpenPlan.setShortcut(QtGui.QApplication.translate("MainWindow", "Ctrl+O", None, QtGui.QApplication.UnicodeUTF8))
        self.actionExit.setText(QtGui.QApplication.translate("MainWindow", "Exit", None, QtGui.QApplication.UnicodeUTF8))
        self.actionExit.setToolTip(QtGui.QApplication.translate("MainWindow", "Click to exit this application", None, QtGui.QApplication.UnicodeUTF8))
        self.actionExit.setStatusTip(QtGui.QApplication.translate("MainWindow", "Exit this application", None, QtGui.QApplication.UnicodeUTF8))
        self.actionExit.setShortcut(QtGui.QApplication.translate("MainWindow", "Ctrl+E", None, QtGui.QApplication.UnicodeUTF8))
        self.actionAbout.setText(QtGui.QApplication.translate("MainWindow", "About", None, QtGui.QApplication.UnicodeUTF8))
        self.actionAbout.setShortcut(QtGui.QApplication.translate("MainWindow", "Ctrl+A", None, QtGui.QApplication.UnicodeUTF8))
        self.actionDocuments.setText(QtGui.QApplication.translate("MainWindow", "Documentation", None, QtGui.QApplication.UnicodeUTF8))
        self.actionDocuments.setShortcut(QtGui.QApplication.translate("MainWindow", "Ctrl+D", None, QtGui.QApplication.UnicodeUTF8))
        self.actionSave.setText(QtGui.QApplication.translate("MainWindow", "Save", None, QtGui.QApplication.UnicodeUTF8))
        self.actionSave.setToolTip(QtGui.QApplication.translate("MainWindow", "Click to save the XML file", None, QtGui.QApplication.UnicodeUTF8))
        self.actionSave.setStatusTip(QtGui.QApplication.translate("MainWindow", "Save the XML file", None, QtGui.QApplication.UnicodeUTF8))
        self.actionSave.setShortcut(QtGui.QApplication.translate("MainWindow", "Ctrl+S", None, QtGui.QApplication.UnicodeUTF8))
        self.actionSaveAs.setText(QtGui.QApplication.translate("MainWindow", "Save As", None, QtGui.QApplication.UnicodeUTF8))
        self.actionDcm2xml.setText(QtGui.QApplication.translate("MainWindow", "Dcm2Xml", None, QtGui.QApplication.UnicodeUTF8))
        self.actionDcm2xml.setIconText(QtGui.QApplication.translate("MainWindow", "Dcm2xml", None, QtGui.QApplication.UnicodeUTF8))
        self.actionDicomTree.setText(QtGui.QApplication.translate("MainWindow", "Dicom Tree", None, QtGui.QApplication.UnicodeUTF8))
        self.actionDicomTree.setIconText(QtGui.QApplication.translate("MainWindow", "Dicom Tree", None, QtGui.QApplication.UnicodeUTF8))
        self.actionDicomTree.setToolTip(QtGui.QApplication.translate("MainWindow", "Click to DICOM Tree", None, QtGui.QApplication.UnicodeUTF8))
        self.actionDicomTree.setStatusTip(QtGui.QApplication.translate("MainWindow", "Click to DICOM Tree", None, QtGui.QApplication.UnicodeUTF8))
        self.actionDcm2xml.setToolTip(QtGui.QApplication.translate("MainWindow", "Convert DICOM-RT to an XML file", None, QtGui.QApplication.UnicodeUTF8))
        self.actionDcm2xml.setStatusTip(QtGui.QApplication.translate("MainWindow", "Convert DICOM-RT to XML ", None, QtGui.QApplication.UnicodeUTF8))
        self.actionLicense.setText(QtGui.QApplication.translate("MainWindow", "License", None, QtGui.QApplication.UnicodeUTF8))
        self.actionLicense.setShortcut(QtGui.QApplication.translate("MainWindow", "Ctrl+L", None, QtGui.QApplication.UnicodeUTF8))

import resources_rc
