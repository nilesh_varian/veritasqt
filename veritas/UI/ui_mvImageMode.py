# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'C:\Users\pmishra\Desktop\research\DMode\QTGuiBWH\mvImageMode.ui'
#
# Created: Thu Jul 03 22:20:40 2014
#      by: pyside-uic 0.2.14 running on PySide 1.2.1
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_imageMode(object):
    def setupUi(self, imageMode):
        imageMode.setObjectName("imageMode")
        imageMode.resize(612, 303)
        imageMode.setMinimumSize(QtCore.QSize(612, 303))
        imageMode.setMaximumSize(QtCore.QSize(612, 303))
        font = QtGui.QFont()
        font.setUnderline(False)
        imageMode.setFont(font)
        self.buttonBox = QtGui.QDialogButtonBox(imageMode)
        self.buttonBox.setGeometry(QtCore.QRect(239, 250, 341, 32))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.label = QtGui.QLabel(imageMode)
        self.label.setGeometry(QtCore.QRect(121, 10, 401, 40))
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setUnderline(True)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.Continuous = QtGui.QRadioButton(imageMode)
        self.Continuous.setGeometry(QtCore.QRect(50, 70, 501, 20))
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setWeight(50)
        font.setUnderline(False)
        font.setBold(False)
        self.Continuous.setFont(font)
        self.Continuous.setChecked(True)
        self.Continuous.setObjectName("Continuous")
        self.Dosimetry = QtGui.QRadioButton(imageMode)
        self.Dosimetry.setGeometry(QtCore.QRect(50, 117, 361, 20))
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setUnderline(False)
        self.Dosimetry.setFont(font)
        self.Dosimetry.setObjectName("Dosimetry")
        self.Highres = QtGui.QRadioButton(imageMode)
        self.Highres.setGeometry(QtCore.QRect(50, 169, 520, 20))
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setUnderline(False)
        self.Highres.setFont(font)
        self.Highres.setObjectName("Highres")
        self.Lowres = QtGui.QRadioButton(imageMode)
        self.Lowres.setGeometry(QtCore.QRect(50, 218, 520, 20))
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setUnderline(False)
        self.Lowres.setFont(font)
        self.Lowres.setObjectName("Lowres")

        self.retranslateUi(imageMode)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL("accepted()"), imageMode.accept)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL("rejected()"), imageMode.reject)
        QtCore.QMetaObject.connectSlotsByName(imageMode)

    def retranslateUi(self, imageMode):
        imageMode.setWindowTitle(QtGui.QApplication.translate("imageMode", "MV Image Mode", None, QtGui.QApplication.UnicodeUTF8))
        self.label.setText(QtGui.QApplication.translate("imageMode", "Image Quality, Single vs. Multiple (Continuous) Images", None, QtGui.QApplication.UnicodeUTF8))
        self.Continuous.setText(QtGui.QApplication.translate("imageMode", "Continuous: Resolution 1024 x 768", None, QtGui.QApplication.UnicodeUTF8))
        self.Dosimetry.setText(QtGui.QApplication.translate("imageMode", "Dosimetry: Continuous, Resolution 1024 x 768", None, QtGui.QApplication.UnicodeUTF8))
        self.Highres.setText(QtGui.QApplication.translate("imageMode", "High Quality: Single Image, Rad Shot, Resolution 1024 x 768", None, QtGui.QApplication.UnicodeUTF8))
        self.Lowres.setText(QtGui.QApplication.translate("imageMode", "Low Dose: Single Image, Resolution 512 x 384", None, QtGui.QApplication.UnicodeUTF8))

