import sys, os, glob, shutil, tempfile
from nose.tools import assert_equal, assert_not_equal, assert_raises

from PySide.QtGui import QApplication
from PySide.QtTest import QTest
from PySide.QtCore import Qt

from mainWindow import MainWindow
import beamon
from imaging import cpImage
# Modules to test

from utils.SetBeam20 import VarianResearchBeam
from lxml import etree
app = QApplication(sys.argv)
import traceback

TEST_DIR = "tests"

EXAMPLE_DIR = "examples"
TEST_OUTPUT_DIR = "test_output"
TEST_INPUT_DIR = "input"

NOSETEST_OUTPUT_PATH = os.path.join(TEST_DIR, TEST_OUTPUT_DIR, "output.xml")
VERITAS_OUTPUT_PATH = os.path.join(TEST_DIR, EXAMPLE_DIR, "output.xml")

TEST_VEL_TOL_OUTPUT_PATH = os.path.join(TEST_DIR, TEST_OUTPUT_DIR, "vel_tol.xml")
VEL_TOL_OUTPUT_PATH = os.path.join(TEST_DIR, EXAMPLE_DIR, "vel_tol.xml")

NOSETEST_IMAGE_PATH = os.path.join(TEST_DIR, TEST_OUTPUT_DIR, "imaging.xml")
VERITAS_IMAGE_PATH = os.path.join(TEST_DIR, EXAMPLE_DIR, "imaging.xml")

IMAGE_INPUT_PATH = os.path.join(TEST_DIR, TEST_INPUT_DIR, "image_input.xml")

NOSETEST_MULTI_PATH = os.path.join(TEST_DIR, TEST_OUTPUT_DIR, "multi_cp.xml")
VERITAS_MULTI_PATH = os.path.join(TEST_DIR, EXAMPLE_DIR, "multi_cp.xml")

CP1_FIELDS = [("Mu", "0"), ("GantryRtn", "10.0"), ("CollRtn", "1.6"), ("CouchVrt", "-5.4"),
              ("CouchLat", "150.0"), ("CouchLng", "-2.7"), ("CouchRtn", "0.1"),
              ("X1", "-16.0"), ("X2", "-14.3"), ("Y1", "14.0"), ("Y2", "4.0"),]

CP2_FIELDS = [("Mu", "2.0")]

CP3_FIELDS = [("Mu", "3.0")]

TOL_FIELDS = [("GantryRtn", "10.0"), ("CollRtn", "1.6"), ("CouchVrt", "-5.4"),
              ("CouchLat", "150.0"), ("CouchLng", "-2.7"), ("CouchRtn", "0.1"),
              ("X1", "-16.0"), ("X2", "-14.3"), ("Y1", "14.0"), ("Y2", "4.0"),]

VEL_FIELDS = [("GantryRtn", "2"), ("CollRtn", "2"), ("CouchVrt", "2"), ("CouchLat", "2"),
              ("CouchLng", "2"), ("CouchRtn", "2"), ("X1", "2"), ("X2", "2"),
              ("Y1", "2"), ("Y2", "2"),]

IMAGE_FIELD1 = {"QLineEdit" :
                {"kvAxisValue": "0.5",
                 "kvdLat": "1", "kvdLng": "2", "kvdVrt": "3", "kvdPitch": "4",
                 "kvsLat": "5", "kvsLng": "6", "kvsVrt": "7", "kvsPitch": "8",
                 "kvShape": "1", "kvFoil": "1"},
                "QSpinBox": {"KiloVolts": "45", "MilliAmperes": "25", "MilliSeconds": "15"},
                "QLabel" : {"kvImageModeLabel": 'DynamicGain'}}

IMAGE_FIELD2 = {"QLineEdit": {"kvAxisValue": "1.5"}}

MLC_MAPPINGS = {"NDS80": 0, "NDS120": 1, "NDS120HD": 2}

# NOTE: VEL TOL HAS TO COME BEFORE BEAMON DUE TO MUTABLE DEFAULTS!!!

def test_main():
    mw = MainWindow()
#    sys.exit(app.exec_())


def test_beamon():
    header = beamon.beamonHeader()


    header.MLCModel.setCurrentIndex(MLC_MAPPINGS.get("NDS80"))

    ui_field_helper(header, "DRate", "62.0")

    index_mappings = {"kV": 0, "MV": 1, "Elec": 2}
    header.EnergyType.setCurrentIndex(index_mappings.get("kV"))
    ui_field_helper(header, "EnergyValue", "2")

    # okWidget = form.buttonBox.button(form.buttonBox.Ok)
    # header.getHeader()
    header.accept()

    form = header.cpWindow
    batch_field_set(form, CP1_FIELDS)
    # Need DRate and Energy as well as subbeam

    form.editAddButton.click()  # click the editAdd button
    form.doneButton.click()

    cp_xml_generate(form, NOSETEST_OUTPUT_PATH)


def test_vel_tol():
    header = beamon.beamonHeader()
    header.MLCModel.setCurrentIndex(MLC_MAPPINGS.get("NDS80"))

    ui_field_helper(header, "DRate", "62.0")

    index_mappings = {"kV": 0, "MV": 1, "Elec": 2}
    header.EnergyType.setCurrentIndex(index_mappings.get("kV"))
    ui_field_helper(header, "EnergyValue", "2")

    vel_table = header.velTable

    batch_field_set(vel_table, VEL_FIELDS)

    vel_widget = vel_table.buttonBox.button(vel_table.buttonBox.Ok)
    vel_widget.click()
    QTest.mouseClick(vel_widget, Qt.LeftButton)

    header.velTableProperties = header.velTable.tableVal

    tol_table = header.tolTable

    batch_field_set(tol_table, TOL_FIELDS)

    tol_widget = tol_table.buttonBox.button(tol_table.buttonBox.Ok)
    tol_widget.click()
    QTest.mouseClick(tol_widget, Qt.LeftButton)

    header.tolTableProperties = header.tolTable.tableVal

    ok_widget = header.buttonBox.button(header.buttonBox.Ok)
    ok_widget.click()
    QTest.mouseClick(ok_widget, Qt.LeftButton)

    header.accept()
    form = header.cpWindow
    batch_field_set(form, CP1_FIELDS)

    form.editAddButton.click()  # click the editAdd button
    form.doneButton.click()
    cp_xml_generate(form, TEST_VEL_TOL_OUTPUT_PATH)

def test_multiple_beamon():
    header = beamon.beamonHeader()

    header.MLCModel.setCurrentIndex(MLC_MAPPINGS.get("NDS80"))

    ui_field_helper(header, "DRate", "62.0")

    index_mappings = {"kV": 0, "MV": 1, "Elec": 2}
    header.EnergyType.setCurrentIndex(index_mappings.get("kV"))
    ui_field_helper(header, "EnergyValue", "2")

    header.accept()

    def add_and_nav(form, fields):
        batch_field_set(form, fields)
        form.editAddButton.click()

    form = header.cpWindow
    add_and_nav(form, CP1_FIELDS)
    add_and_nav(form, CP2_FIELDS)
    add_and_nav(form, CP3_FIELDS)

    form.doneButton.click()

    cp_xml_generate(form, NOSETEST_MULTI_PATH)

def test_imaging():
    cp_image = cpImage(None, IMAGE_INPUT_PATH, None)
    cp_image.selectKV.click()

    image_fields_helper(cp_image, IMAGE_FIELD1)
    cp_image.nextButton.click()

    image_fields_helper(cp_image, IMAGE_FIELD2)
    cp_image.nextButton.click()

    # Not a click so that no user input is required
    cp_image.doneCP(NOSETEST_IMAGE_PATH)


def compare_xml_output(test_output, control_output):
    with open(test_output, "r") as f:
        export_xml = f.read()

    with open(control_output, "r") as f:
        output_xml = f.read()

    export_node_list = export_xml.split()
    export_xml = ''.join(export_node_list)

    output_node_list = output_xml.split()
    output_xml = ''.join(output_node_list)
    assert_equal(output_xml, export_xml)


def test_beamon_xml():
    compare_xml_output(NOSETEST_OUTPUT_PATH, VERITAS_OUTPUT_PATH)

def test_vel_tol_xml():
    compare_xml_output(VEL_TOL_OUTPUT_PATH, TEST_VEL_TOL_OUTPUT_PATH)


def test_multi_xml():
    compare_xml_output(NOSETEST_MULTI_PATH, VERITAS_MULTI_PATH)

def test_imaging_xml():
    compare_xml_output(NOSETEST_IMAGE_PATH, VERITAS_IMAGE_PATH)

def ui_field_helper(form, field, value):
    field_name = getattr(form, field)
    field_name.setText(value)
    assert_equal(field_name.text(), value,
                 "form.{0} is {1}".format(field, field_name.text()))

def set_spinboxes(form, field_pairs):
    for name, value in field_pairs.items():
        field_name = getattr(form, name)
        field_name.setValue(int(value))

def set_labels(form, field_pairs):
    for name, value in field_pairs.items():
        field_name = getattr(form, name)
        field_name.setText(value)



def image_fields_helper(image, field_dict):
    for field_type, field_pairs in field_dict.items():
        if field_type == "QLineEdit":
            batch_field_set(image, tuple(field_pairs.items()))
        elif field_type == "QSpinBox":
            set_spinboxes(image, field_pairs)
        elif field_type == "QLabel":
            set_labels(image, field_pairs)


def batch_field_set(ui_window, zipped_forms):
    map(lambda x: ui_field_helper(ui_window, x[0], x[1]), zipped_forms)

def cp_xml_generate(form, output_file):
    cpdata = form.cpdata
    root = cpdata.create_xml()
    root_tag = 'VarianResearchBeam'
    root_obj = VarianResearchBeam.factory()
    root_obj.build(root)
    out_file = open(output_file, "w")
    root_obj.export(out_file, 0, name_=root_tag,
                    namespacedef_='', pretty_print=True)

def validate_xml(form):
    cpdata = form.cpdata
    root = cpdata.create_xml()

    # Validating xml before save it
    with open("TrueBeam_Developer_Mode_Schema_1.6.xsd", "r") as fr:
        schema_root = etree.XML(fr.read())
        fr.close()
    schema = etree.XMLSchema(schema_root)
    parser = etree.XMLParser(schema = schema)
    xml_string = etree.tostring(root)

    try:
        root = etree.fromstring(xml_string, parser)
    except:
        raise Exception("lxml.etree.XMLSyntaxError")

def validate_image_xml(root):
    # Validating xml before save it
    with open("TrueBeam_Developer_Mode_Schema_1.6.xsd", "r") as fr:
        schema_root = etree.XML(fr.read())
        fr.close()
    schema = etree.XMLSchema(schema_root)
    parser = etree.XMLParser(schema = schema)
    xml_string = etree.tostring(root)
    import pdb; pdb.set_trace()

    try:
        root = etree.fromstring(xml_string, parser)
    except:
        raise Exception("lxml.etree.XMLSyntaxError")

def image_xml_generate(root, output_file):
    root_tag = 'VarianResearchBeam'
    root_obj = VarianResearchBeam.factory()
    root_obj.build(root)

    out_file = open(output_file, "w")
    root_obj.export(out_file, 0, name_=root_tag,
                    namespacedef_='', pretty_print=True)

# Actual test cases
def test_example1():
    """
    Just beam on for 100 MU
    Example 1: Just Beam On: One of the simplest XML beams.
    """
    header = beamon.beamonHeader()
    header.MLCModel.setCurrentIndex(MLC_MAPPINGS.get("NDS120HD"))

    ui_field_helper(header, "DRate", "300")

    index_mappings = {"kV": 0, "MV": 1, "Elec": 2}
    header.EnergyType.setCurrentIndex(index_mappings.get("MV"))
    ui_field_helper(header, "EnergyValue", "6")

    # okWidget = form.buttonBox.button(form.buttonBox.Ok)
    # header.getHeader()
    header.accept()

    def add_and_nav(form, fields):
        form.cpdata.add_cp(**dict(fields))

    form = header.cpWindow
    form.controlPoints = [form.controlPoints[0]]
    form.cpdata.cplist = [form.cpdata.cplist[0]]
    add_and_nav(form, [("Mu", "0"),])
    add_and_nav(form, [("Mu", "100"),])

    form.editAddButton.click()  # click the editAdd button
    form.doneButton.click()

    validate_xml(form)
    cp_xml_generate(form, NOSETEST_OUTPUT_PATH)


def test_example2():
    """
    200 MU at 400MU/min dose rate static field.
    Requires that some axes are positioned within tolerance prior to beam on.
    Example 2: Adding static positions and tolerances.
    """
    header = beamon.beamonHeader()
    header.MLCModel.setCurrentIndex(MLC_MAPPINGS.get("NDS120HD"))

    ui_field_helper(header, "DRate", "400")

    index_mappings = {"kV": 0, "MV": 1, "Elec": 2}
    header.EnergyType.setCurrentIndex(index_mappings.get("MV"))
    ui_field_helper(header, "EnergyValue", "6")


    tol_table = header.tolTable

    batch_field_set(tol_table, [("GantryRtn", "0.30"), ("CollRtn", "0.0"), ("CouchVrt", "2.00"),
                                ("CouchLat", "0.0"), ("CouchLng", "1000.00"), ("CouchRtn", "3.50"),])

    tol_widget = tol_table.buttonBox.button(tol_table.buttonBox.Ok)
    tol_widget.click()
    QTest.mouseClick(tol_widget, Qt.LeftButton)

    header.tolTableProperties = header.tolTable.tableVal

    ok_widget = header.buttonBox.button(header.buttonBox.Ok)
    ok_widget.click()
    QTest.mouseClick(ok_widget, Qt.LeftButton)

    header.accept()

    def add_and_nav(form, fields):
        form.cpdata.add_cp(**dict(fields))

    form = header.cpWindow
    form.controlPoints = [form.controlPoints[0]]
    form.cpdata.cplist = [form.cpdata.cplist[0]]
    add_and_nav(form, [("Mu", "0"), ("GantryRtn", "221.0"), ("CollRtn", "180.0"), ("CouchVrt", "90.54"), ("CouchLng", "100.00"), ("CouchRtn", "180.0"), ("Y1", "5.31"), ("Y2", "4.81"), ("X1", "7.18"), ("X2", "7.18"), ("AB", ("0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.726 0.926 1.626 0.175 0.475 0.475 0.075 0.525 0.625 0.325 1.025 1.825 2.025 1.625 1.025 0.525 0.326 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0","0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.675 0.875 1.575 1.125 1.725 2.225 2.725 3.225 3.425 1.025 3.325 2.925 0.425 1.825 1.425 0.075 0.275 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0")), ])
    add_and_nav(form, [("Mu", "200"),])

    form.editAddButton.click()  # click the editAdd button
    form.doneButton.click()

    validate_xml(form)
    cp_xml_generate(form, NOSETEST_OUTPUT_PATH)


def test_example3():
    """
    Two static MLC fields delivered at two different gantry angles. First field delivers 110 MU, second field 72 MU
    Example 3: Adding static fields with MLC.
    """
    header = beamon.beamonHeader()
    header.MLCModel.setCurrentIndex(MLC_MAPPINGS.get("NDS120HD"))

    ui_field_helper(header, "DRate", "400")

    index_mappings = {"kV": 0, "MV": 1, "Elec": 2}
    header.EnergyType.setCurrentIndex(index_mappings.get("MV"))
    ui_field_helper(header, "EnergyValue", "6")

    header.accept()

    def add_and_nav(form, fields):
        form.cpdata.add_cp(**dict(fields))

    form = header.cpWindow
    form.controlPoints = [form.controlPoints[0]]
    form.cpdata.cplist = [form.cpdata.cplist[0]]
    add_and_nav(form, [("Mu", "0"), ("GantryRtn", "180.00"), ("CollRtn", "180.0"), ("CouchVrt", "90.0"), ("CouchLng", "100.00"), ("CouchRtn", "180.0"), ("Y1", "5.31"), ("Y2", "4.81"), ("X1", "7.18"), ("X2", "7.18"), ("AB", ("0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.726 0.926 1.626 0.175 0.475 0.475 0.075 0.525 0.625 0.325 1.025 1.825 2.025 1.625 1.025 0.525 0.326 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0","0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.675 0.875 1.575 1.125 1.725 2.225 2.725 3.225 3.425 1.025 3.325 2.925 0.425 1.825 1.425 0.075 0.275 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0")),])

    add_and_nav(form, [("Mu", "110"),])

    add_and_nav(form, [("GantryRtn", "90.00"), ("AB", ("0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 -0.218 0.115 0.826 0.826 1.666 0.215 0.775 0.975 0.275 0.525 0.825 0.725 1.025 1.725 2.325 2.025 1.425 0.720 0.964 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0","0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.988 0.235 0.775 0.995 1.475 1.525 1.725 2.825 2.525 3.625 3.825 1.325 3.725 3.125 0.725 1.925 1.620 -0.170 -0.550 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0")),])

    add_and_nav(form, [("Mu", "182"),])
    form.editAddButton.click()  # click the editAdd button
    form.doneButton.click()

    validate_xml(form)
    cp_xml_generate(form, NOSETEST_OUTPUT_PATH)


def test_example4():
    """
    RapidArc with collimator rotation. Gantry rotates 360, MLC changes shape and also collimator rotates.
    Example 4: A dynamic beam with simultaneous MU delivery and motions.
    """
    header = beamon.beamonHeader()
    header.MLCModel.setCurrentIndex(MLC_MAPPINGS.get("NDS120"))

    ui_field_helper(header, "DRate", "400")

    index_mappings = {"kV": 0, "MV": 1, "Elec": 2}
    header.EnergyType.setCurrentIndex(index_mappings.get("MV"))
    ui_field_helper(header, "EnergyValue", "6")

    header.accept()

    def add_and_nav(form, fields):
        form.cpdata.add_cp(**dict(fields))

    form = header.cpWindow
    form.controlPoints = [form.controlPoints[0]]
    form.cpdata.cplist = [form.cpdata.cplist[0]]
    add_and_nav(form, [("Mu", "0"), ("GantryRtn", "360.00"), ("CollRtn", "177.05"), ("CouchVrt", "90.0"), ("CouchLat", "100.00"), ("CouchLng", "100.00"), ("CouchRtn", "180.0"), ("Y1", "5.31"), ("Y2", "4.81"), ("X1", "7.18"), ("X2", "7.18"),("AB", ("0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.726 0.926 1.626 -0.175 -0.575 -0.495 -0.175 0.725 0.825 0.465 1.025 1.825 2.025 1.625 1.025 0.525 0.326 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0", "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 -0.550 -0.875 -1.465 1.125 1.835 2.125 2.555 3.625 3.775 1.225 4.325 2.925 1.325 1.725 1.525 -0.175 -0.275 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0")),])

    add_and_nav(form, [("Mu", "2.13"), ("GantryRtn", "357.03"), ("CollRtn", "177.54"),("AB", ("0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.726 0.895 1.626 -0.175 -0.466 -0.475 -0.075 0.525 0.625 0.325 1.025 1.825 2.025 1.625 1.025 0.525 0.326 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0","0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 -0.674 -0.825 -1.575 1.125 1.725 2.225 2.670 3.200 3.425 1.025 3.325 2.925 0.425 1.825 1.425 -0.075 -0.275 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0")),])

    add_and_nav(form, [("Mu", "6.09"), ("GantryRtn", "355.07"),("AB", ("0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.725 0.851 1.626 -0.175 -0.375 -0.075 -0.075 0.525 0.725 -0.475 1.125 1.025 2.025 1.525 1.025 0.525 0.225 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0", "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 -0.674 -0.8 -1.575 1.125 1.725 2.225 2.625 3.125 3.325 1.425 3.225 2.925 0.818 1.825 0.925 -0.175 -0.174 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0")),])

    add_and_nav(form, [("Mu", "590.88"), ("GantryRtn", "0.0"),("AB", ("0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1.326 1.526 1.426 1.918 3.125 4.125 4.325 -2.675 -2.875 -1.975 -2.275 -2.275 2.225 1.525 1.025 0.325 0.826 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0", "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 -1.275 -1.475 -1.375 0.825 -2.975 -4.074 -4.274 3.125 3.425 3.525 3.425 3.125 -1.375 -0.482 1.425 0.325 -0.775 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0")),])

    form.editAddButton.click()  # click the editAdd button
    form.doneButton.click()

    validate_xml(form)
    cp_xml_generate(form, NOSETEST_OUTPUT_PATH)


def test_example5():
    """
    RapidArc with collimator rotation. Gantry rotates 360, MLC changes shape and also collimator rotates.
    Example 5: Specifying Dose Rate and Velocity Limits
    """
    header = beamon.beamonHeader()
    header.MLCModel.setCurrentIndex(MLC_MAPPINGS.get("NDS120"))

    ui_field_helper(header, "DRate", "520")

    index_mappings = {"kV": 0, "MV": 1, "Elec": 2}
    header.EnergyType.setCurrentIndex(index_mappings.get("MV"))
    ui_field_helper(header, "EnergyValue", "6")

    # okWidget = form.buttonBox.button(form.buttonBox.Ok)
    # header.getHeader()
    header.accept()

    def add_and_nav(form, fields):
        form.cpdata.add_cp(**dict(fields))

    form = header.cpWindow
    form.controlPoints = [form.controlPoints[0]]
    form.cpdata.cplist = [form.cpdata.cplist[0]]
    add_and_nav(form, [("Mu", "0"), ("GantryRtn", "360.00"), ("CollRtn", "177.05"), ("CouchVrt", "90.0"), ("CouchLat", "100.00"), ("CouchLng", "100.00"), ("CouchRtn", "180.0"), ("Y1", "5.31"), ("Y2", "4.81"), ("X1", "7.18"), ("X2", "7.18"), ("AB", ("0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.726 0.926 1.626 -0.175 -0.575 -0.495 -0.175 0.725 0.825 0.465 1.025 1.825 2.025 1.625 1.025 0.525 0.326 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0", "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 -0.550 -0.875 -1.465 1.125 1.835 2.125 2.555 3.625 3.775 1.225 4.325 2.925 1.325 1.725 1.525 -0.175 -0.275 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0")),])

    form.editAddButton.click()  # click the editAdd button
    form.doneButton.click()

    validate_xml(form)
    cp_xml_generate(form, NOSETEST_OUTPUT_PATH)


def test_example6():
    """
    Arc Beam, delivers 180 MU over 200 degree angle.
    5 arc regions at different MU/deg.
    MV images taken at 10 MU and 172.79 MU
    KV images taken at 20 MU and 120 MU
    Example 6: Acquiring kV and MV images concurrent with MV beam.
    """
    header = beamon.beamonHeader()
    header.MLCModel.setCurrentIndex(MLC_MAPPINGS.get("NDS120"))

    ui_field_helper(header, "DRate", "520")

    index_mappings = {"kV": 0, "MV": 1, "Elec": 2}
    header.EnergyType.setCurrentIndex(index_mappings.get("MV"))
    ui_field_helper(header, "EnergyValue", "6")

    vel_table = header.velTable

    batch_field_set(vel_table, [("GantryRtn", "2.50"), ("CollRtn", "0.00"), ("CouchVrt", "0.00"),
                                ("CouchLat", "0.00"), ("CouchLng", "0.00"), ("CouchRtn", "0.00"),])

    vel_widget = vel_table.buttonBox.button(vel_table.buttonBox.Ok)
    vel_widget.click()
    QTest.mouseClick(vel_widget, Qt.LeftButton)

    header.velTableProperties = header.velTable.tableVal

    # okWidget = form.buttonBox.button(form.buttonBox.Ok)
    # header.getHeader()
    header.accept()

    def add_and_nav(form, fields):
        form.cpdata.add_cp(**dict(fields))

    form = header.cpWindow
    form.controlPoints = [form.controlPoints[0]]
    form.cpdata.cplist = [form.cpdata.cplist[0]]
    add_and_nav(form, [("Mu", "0"), ("GantryRtn", "360.00"), ("CollRtn", "177.05"), ("CouchVrt", "90.0"), ("CouchLat", "100.00"), ("CouchLng", "100.00"), ("CouchRtn", "180.0"), ("Y1", "5.31"), ("Y2", "4.81"), ("X1", "7.18"), ("X2", "7.18"), ("AB",("0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.726 0.926 1.626 -0.175 -0.575 -0.495 -0.175 0.725 0.825 0.465 1.025 1.825 2.025 1.625 1.025 0.525 0.326 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0", "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 -0.550 -0.875 -1.465 1.125 1.835 2.125 2.555 3.625 3.775 1.225 4.325 2.925 1.325 1.725 1.525 -0.175 -0.275 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0")),])

    form.editAddButton.click()  # click the editAdd button
    form.doneButton.click()

    validate_xml(form)
    cp_xml_generate(form, NOSETEST_OUTPUT_PATH)


def test_example7():
    """
    Two static MLC fields. First delivers 110 MU (3.2 for image + 106.8)
    Second field delivers 72 MU (3.2 for image + 68.8)
    Orthogonal MV+KV images taken before each field
    Example 7: Dummy control points facilitate specifying where exactly images are to be acquired.
    """

    header = beamon.beamonHeader()
    header.MLCModel.setCurrentIndex(MLC_MAPPINGS.get("NDS120HD"))

    ui_field_helper(header, "DRate", "600")

    index_mappings = {"kV": 0, "MV": 1, "Elec": 2}
    header.EnergyType.setCurrentIndex(index_mappings.get("MV"))
    ui_field_helper(header, "EnergyValue", "6")

    # okWidget = form.buttonBox.button(form.buttonBox.Ok)
    # header.getHeader()
    header.accept()

    def add_and_nav(form, fields):
        form.cpdata.add_cp(**dict(fields))

    form = header.cpWindow
    form.controlPoints = [form.controlPoints[0]]
    form.cpdata.cplist = [form.cpdata.cplist[0]]
    add_and_nav(form, [("Mu", "0"), ("GantryRtn", "180.00"), ("CollRtn", "180.00"), ("CouchVrt", "90.0"), ("CouchLat", "100.00"), ("CouchLng", "100.00"), ("CouchRtn", "180.0"), ("Y1", "5.00"), ("Y2", "5.00"), ("X1", "6.00"), ("X2", "6.00"), ("AB", ("0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 5.000 5.000 5.000 5.000 5.000 5.000 5.000 5.000 5.000 5.000 5.000 5.000 5.000 5.000 5.000 5.000 5.000 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0", "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 5.000 5.000 5.000 5.000 5.000 5.000 5.000 5.000 5.000 5.000 5.000 5.000 5.000 5.000 5.000 5.000 5.000 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0")),])

    add_and_nav(form, [("Mu", "3.2"), ])

    add_and_nav(form, [("Y1", "5.31"), ("Y2", "4.81"), ("X1", "7.18"), ("X2", "7.18"), ("AB", ("0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.726 0.926 1.626 0.175 0.475 0.475 0.075 0.525 0.625 0.325 1.025 1.825 2.025 1.625 1.025 0.525 0.326 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0", "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.675 0.875 1.575 1.125 1.725 2.225 2.725 3.225 3.425 1.025 3.325 2.925 0.425 1.825 1.425 0.075 0.275 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0")),])

    add_and_nav(form, [("Mu", "110"), ])

    add_and_nav(form, [("GantryRtn", "90.00"), ("Y1", "5.00"), ("Y2", "5.00"), ("X1", "6.00"), ("X2", "6.00"), ("AB", ("0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 5.000 5.000 5.000 5.000 5.000 5.000 5.000 5.000 5.000 5.000 5.000 5.000 5.000 5.000 5.000 5.000 5.000 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0", "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 5.000 5.000 5.000 5.000 5.000 5.000 5.000 5.000 5.000 5.000 5.000 5.000 5.000 5.000 5.000 5.000 5.000 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0")),])

    add_and_nav(form, [("Mu", "113.2"), ])

    add_and_nav(form, [("Y1", "5.31"), ("Y2", "4.81"), ("X1", "7.18"), ("X2", "7.18"), ("AB", ("0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1.826 2.426 2.126 2.126 0.025 0.825 1.225 1.525 1.625 2.625 2.825 2.625 2.025 1.525 1.025 0.525 0.025 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0", "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1.775 2.375 2.075 2.075 0.125 0.325 0.225 0.175 1.275 1.475 0.825 1.425 2.325 2.425 2.225 1.525 0.525 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0")),])

    add_and_nav(form, [("Mu", "182"), ])

    form.editAddButton.click()  # click the editAdd button
    form.doneButton.click()

    #cp_xml_generate(form, NOSETEST_OUTPUT_PATH)
    image_points = []

    def add_image_points(point):
        image_points.append(point)

    point1 =  {'kvImageModeLabel': 'HighQuality', 'kvAxisValue': '1', 'kvShape': '', 'kvdLng': '', 'kvAxis': 'Mu', 'kvdPitch': '', 'MilliAmperes': '50',
                'kvsLng': '', 'kvAxisValueStop': '', 'kvsPitch': '', 'kvsLat': '', 'kvdVrt': '', 'kvdLat': '', 'kvsVrt': '', 'KiloVolts': '80', 'kvFoil': '', 'MilliSeconds': '10'}, None
    add_image_points(point1)

    point2 = None, {'mvAxisValueStop': '', 'mvdPitch': '', 'mvAxis': 'Mu', 'mvImageModeLabel': 'Highres', 'mvdVrt': '', 'mvAxisValue': '2', 'mvdLat': '', 'mvdLng': ''}
    add_image_points(point2)

    point3 = {'kvImageModeLabel': 'HighQuality', 'kvAxisValue': '7', 'kvShape': '', 'kvdLng': '', 'kvAxis': 'Mu', 'kvdPitch': '', 'MilliAmperes': '50',
                'kvsLng': '', 'kvAxisValueStop': '', 'kvsPitch': '', 'kvsLat': '', 'kvdVrt': '', 'kvdLat': '', 'kvsVrt': '', 'KiloVolts': '80', 'kvFoil': '', 'MilliSeconds': '10'}, None
    add_image_points(point3)

    point4 = None, {'mvAxisValueStop': '', 'mvdPitch': '', 'mvAxis': 'Mu', 'mvImageModeLabel': 'Highres', 'mvdVrt': '', 'mvAxisValue': '8', 'mvdLat': '', 'mvdLng': ''}
    add_image_points(point4)

    cp_image = cpImage(None, IMAGE_INPUT_PATH, None)
    cp_image.image_only = False

    cpdata = form.cpdata
    cp_image.root = cpdata.create_xml()
    cp_image.sbeam = cp_image.root.getchildren()[0]

    # currently, this is broken, since previously it assumed
    # the tree was updated live, whereas now it's updated
    # after the cps have been filled out
    cp_image._set_image_params()
    for index, value in enumerate(image_points):
        kv_cp, mv_cp = value
        if kv_cp:
            cur_frac_cp = cp_image._generate_cp(kv_cp, "kvAxis", "kvAxisValue")
            cp_image.kvImagingNode(cp_image.root, kv_cp, cur_frac_cp, index)
        elif mv_cp:
            cur_frac_cp = cp_image._generate_cp(mv_cp, "mvAxis", "mvAxisValue")
            cp_image.mvImagingNode(cp_image.root, mv_cp, cur_frac_cp, index)

    validate_image_xml(cp_image.root)
    image_xml_generate(cp_image.root, NOSETEST_OUTPUT_PATH)

def test_example9():
    """
    Arc Beam, delivers 180 MU over 200 degree angle (0 to 200 deg).
    5 arc regions at different MU/deg.
    Continuous KV images are taken in the 40 to 90 degree region of the arc
    Example 9: Taking continuous images.
    """
    header = beamon.beamonHeader()
    header.MLCModel.setCurrentIndex(MLC_MAPPINGS.get("NDS120HD"))

    ui_field_helper(header, "DRate", "600")

    index_mappings = {"kV": 0, "MV": 1, "Elec": 2}
    header.EnergyType.setCurrentIndex(index_mappings.get("MV"))
    ui_field_helper(header, "EnergyValue", "6")

    # okWidget = form.buttonBox.button(form.buttonBox.Ok)
    # header.getHeader()
    header.accept()

    def add_and_nav(form, fields):
        form.cpdata.add_cp(**dict(fields))

    form = header.cpWindow
    form.controlPoints = [form.controlPoints[0]]
    form.cpdata.cplist = [form.cpdata.cplist[0]]
    add_and_nav(form, [("Mu", "0"), ("GantryRtn", "0.00"), ])
    add_and_nav(form, [("Mu", "40"), ("GantryRtn", "40.00"), ])
    add_and_nav(form, [("Mu", "100"), ("GantryRtn", "80.00"), ])
    add_and_nav(form, [("Mu", "140"), ("GantryRtn", "100.00"), ])
    add_and_nav(form, [("Mu", "170"), ("GantryRtn", "160.00"), ])
    add_and_nav(form, [("Mu", "180"), ("GantryRtn", "200.00"), ])

    form.editAddButton.click()  # click the editAdd button
    form.doneButton.click()

    #cp_xml_generate(form, NOSETEST_OUTPUT_PATH)
    image_points = []

    def add_image_points(point):
        image_points.append(point)

    point1 =  {'kvImageModeLabel': 'DynamicGain', 'kvAxisValue': '0', 'kvShape': '', 'kvdLng': '0', 'kvAxis': 'Mu', 'kvdPitch': '0', 'MilliAmperes': '50',
                'kvsLng': '0', 'kvAxisValueStop': '', 'kvsPitch': '0', 'kvsLat': '0', 'kvdVrt': '-70', 'kvdLat': '0', 'kvsVrt': '90', 'KiloVolts': '80', 'kvFoil': '', 'MilliSeconds': '10'}, None
    add_image_points(point1)

    point2 = {'kvImageModeLabel': 'DynamicGainFluoro', 'kvAxisValue': '40', 'kvShape': '1', 'kvdLng': '', 'kvAxis': 'Mu', 'kvdPitch': '', 'MilliAmperes': '20',
                'kvsLng': '', 'kvAxisValueStop': '90', 'kvsPitch': '', 'kvsLat': '', 'kvdVrt': '', 'kvdLat': '', 'kvsVrt': '', 'KiloVolts': '100', 'kvFoil': '1', 'MilliSeconds': '20'}, None
    add_image_points(point2)

    cp_image = cpImage(None, IMAGE_INPUT_PATH, None)
    cp_image.image_only = False

    cpdata = form.cpdata
    cp_image.root = cpdata.create_xml()
    cp_image.sbeam = cp_image.root.getchildren()[0]

    # currently, this is broken, since previously it assumed
    # the tree was updated live, whereas now it's updated
    # after the cps have been filled out
    cp_image._set_image_params()
    for index, value in enumerate(image_points):
        kv_cp, mv_cp = value
        if kv_cp:
            cur_frac_cp = cp_image._generate_cp(kv_cp, "kvAxis", "kvAxisValue")
            cp_image.kvImagingNode(cp_image.root, kv_cp, cur_frac_cp, index)
        elif mv_cp:
            cur_frac_cp = cp_image._generate_cp(mv_cp, "mvAxis", "mvAxisValue")
            cp_image.mvImagingNode(cp_image.root, mv_cp, cur_frac_cp, index)

    validate_image_xml(cp_image.root)
    image_xml_generate(cp_image.root, NOSETEST_OUTPUT_PATH)

def test_example10():
    """
    Rotate gantry 368 degrees and take KV images to be used for Cone Beam Reconstruction (CBCT).
    Example 10: Taking images only (i.e. imaging only, no therapeutic beam)
    """
    header = beamon.beamonHeader()
    header.MLCModel.setCurrentIndex(MLC_MAPPINGS.get("NDS120HD"))

    ui_field_helper(header, "DRate", "0")

    index_mappings = {"kV": 0, "MV": 1, "Elec": 2}
    header.EnergyType.setCurrentIndex(index_mappings.get("kV"))
    ui_field_helper(header, "EnergyValue", "0")

    # okWidget = form.buttonBox.button(form.buttonBox.Ok)
    # header.getHeader()
    header.accept()

    def add_and_nav(form, fields):
        form.cpdata.add_cp(**dict(fields))

    form = header.cpWindow
    form.controlPoints = [form.controlPoints[0]]
    form.cpdata.cplist = [form.cpdata.cplist[0]]
    add_and_nav(form, [("Mu", "0"), ("GantryRtn", "364.00"),])
    add_and_nav(form, [("Mu", "4"), ("GantryRtn", "4"), ])

    form.editAddButton.click()  # click the editAdd button
    form.doneButton.click()

    #cp_xml_generate(form, NOSETEST_OUTPUT_PATH)
    image_points = []

    def add_image_points(point):
        image_points.append(point)

    point1 =  {'kvImageModeLabel': 'DynamicGainFluoro', 'kvAxisValue': '0', 'kvShape': '1', 'kvdLng': '0', 'kvAxis': 'Mu', 'kvdPitch': '0', 'MilliAmperes': '40',
                'kvsLng': '0', 'kvAxisValueStop': '1', 'kvsPitch': '0', 'kvsLat': '0', 'kvdVrt': '-50', 'kvdLat': '0', 'kvsVrt': '100', 'KiloVolts': '100', 'kvFoil': '1', 'MilliSeconds': '10'}, None
    add_image_points(point1)

    cp_image = cpImage(None, IMAGE_INPUT_PATH, None)
    cp_image.image_only = True

    cpdata = form.cpdata
    cp_image.root = cpdata.create_xml()
    cp_image.sbeam = cp_image.root.getchildren()[0]

    # currently, this is broken, since previously it assumed
    # the tree was updated live, whereas now it's updated
    # after the cps have been filled out
    cp_image._set_image_params()
    for index, value in enumerate(image_points):
        kv_cp, mv_cp = value
        if kv_cp:
            cur_frac_cp = cp_image._generate_cp(kv_cp, "kvAxis", "kvAxisValue")
            cp_image.kvImagingNode(cp_image.root, kv_cp, cur_frac_cp, index)
        elif mv_cp:
            cur_frac_cp = cp_image._generate_cp(mv_cp, "mvAxis", "mvAxisValue")
            cp_image.mvImagingNode(cp_image.root, mv_cp, cur_frac_cp, index)

    validate_image_xml(cp_image.root)
    image_xml_generate(cp_image.root, NOSETEST_OUTPUT_PATH)

def test_example11():
    """
    Takes MV image in outside treatment mode, thus leaving the MU
    unspecified. Min MU required for image requested will be
    pulled out of configuration for the image requested
    Example 11: Letting the control system deliver only as much MV MU as necessary to acquire image(s)
    """
    header = beamon.beamonHeader()
    header.MLCModel.setCurrentIndex(MLC_MAPPINGS.get("NDS120HD"))

    ui_field_helper(header, "DRate", "300")

    index_mappings = {"kV": 0, "MV": 1, "Elec": 2}
    header.EnergyType.setCurrentIndex(index_mappings.get("kV"))
    ui_field_helper(header, "EnergyValue", "6")

    # okWidget = form.buttonBox.button(form.buttonBox.Ok)
    # header.getHeader()
    header.accept()

    def add_and_nav(form, fields):
        form.cpdata.add_cp(**dict(fields))

    form = header.cpWindow
    form.controlPoints = [form.controlPoints[0]]
    form.cpdata.cplist = [form.cpdata.cplist[0]]
    add_and_nav(form, [("Mu", "0"), ])

    form.editAddButton.click()  # click the editAdd button
    form.doneButton.click()

    #cp_xml_generate(form, NOSETEST_OUTPUT_PATH)
    image_points = []

    def add_image_points(point):
        image_points.append(point)

    point1 = None, {'mvAxisValueStop': '', 'mvdPitch': '0', 'mvAxis': 'Mu', 'mvImageModeLabel': 'Highres', 'mvdVrt': '-50', 'mvAxisValue': '0', 'mvdLat': '0', 'mvdLng': '0'}
    add_image_points(point1)

    cp_image = cpImage(None, IMAGE_INPUT_PATH, None)
    cp_image.image_only = True

    cpdata = form.cpdata
    cp_image.root = cpdata.create_xml()
    cp_image.sbeam = cp_image.root.getchildren()[0]

    # currently, this is broken, since previously it assumed
    # the tree was updated live, whereas now it's updated
    # after the cps have been filled out
    cp_image._set_image_params()
    for index, value in enumerate(image_points):
        kv_cp, mv_cp = value
        if kv_cp:
            cur_frac_cp = cp_image._generate_cp(kv_cp, "kvAxis", "kvAxisValue")
            cp_image.kvImagingNode(cp_image.root, kv_cp, cur_frac_cp, index)
        elif mv_cp:
            cur_frac_cp = cp_image._generate_cp(mv_cp, "mvAxis", "mvAxisValue")
            cp_image.mvImagingNode(cp_image.root, mv_cp, cur_frac_cp, index)

    validate_image_xml(cp_image.root)
    image_xml_generate(cp_image.root, NOSETEST_OUTPUT_PATH)


def test_example_12_gated_kv():
    """
    Rotate gantry 368 degrees and take KV images to be used
    for Cone Beam Reconstruction (CBCT).
    However take images only when gate is open and slow down
    the gantry to take enough images for CBCT
    Example 12: Gated kV images
    """
    header = beamon.beamonHeader()
    header.MLCModel.setCurrentIndex(MLC_MAPPINGS.get("NDS120HD"))

    ui_field_helper(header, "DRate", "0")

    index_mappings = {"kV": 0, "MV": 1, "Elec": 2}
    header.EnergyType.setCurrentIndex(index_mappings.get("kV"))
    ui_field_helper(header, "EnergyValue", "0")

    # okWidget = form.buttonBox.button(form.buttonBox.Ok)
    # header.getHeader()
    header.accept()

    def add_and_nav(form, fields):
        form.cpdata.add_cp(**dict(fields))

    form = header.cpWindow
    form.controlPoints = [form.controlPoints[0]]
    form.cpdata.cplist = [form.cpdata.cplist[0]]
    add_and_nav(form, [("Mu", "0"), ("GantryRtn", "360"),])
    add_and_nav(form, [("Mu", "4"), ("GantryRtn", "4"), ])

    form.editAddButton.click()  # click the editAdd button
    form.doneButton.click()

    #cp_xml_generate(form, NOSETEST_OUTPUT_PATH)
    image_points = []

    def add_image_points(point):
        image_points.append(point)

    point1 =  {'kvImageModeLabel': 'DynamicGainFluoro', 'kvAxisValue': '0', 'kvShape': '1', 'kvdLng': '0', 'kvAxis': 'Mu', 'kvdPitch': '0', 'MilliAmperes': '40',
                'kvsLng': '0', 'kvAxisValueStop': '1', 'kvsPitch': '0', 'kvsLat': '0', 'kvdVrt': '-50', 'kvdLat': '0', 'kvsVrt': '100', 'KiloVolts': '100', 'kvFoil': '1', 'MilliSeconds': '10'}, None
    add_image_points(point1)

    cp_image = cpImage(None, IMAGE_INPUT_PATH, None)
    cp_image.image_only = True

    cpdata = form.cpdata
    cp_image.root = cpdata.create_xml()
    cp_image.sbeam = cp_image.root.getchildren()[0]

    # currently, this is broken, since previously it assumed
    # the tree was updated live, whereas now it's updated
    # after the cps have been filled out
    cp_image._set_image_params()
    for index, value in enumerate(image_points):
        kv_cp, mv_cp = value
        if kv_cp:
            cur_frac_cp = cp_image._generate_cp(kv_cp, "kvAxis", "kvAxisValue")
            cp_image.kvImagingNode(cp_image.root, kv_cp, cur_frac_cp, index)
        elif mv_cp:
            cur_frac_cp = cp_image._generate_cp(mv_cp, "mvAxis", "mvAxisValue")
            cp_image.mvImagingNode(cp_image.root, mv_cp, cur_frac_cp, index)

    validate_image_xml(cp_image.root)
    image_xml_generate(cp_image.root, NOSETEST_OUTPUT_PATH)


def test_example_12_gated_mv():
    """
    Arc Beam, delivers 180 MU over 200 degree angle (0 to 200 deg).
    5 arc regions at different MU/deg.
    Continuous KV images are taken in the 40 to 90 degree region of the arc
    However, both the MV beam as well as the imaging KV beam are gated
    Example 12: Gating the MV beam.
    """
    header = beamon.beamonHeader()
    header.MLCModel.setCurrentIndex(MLC_MAPPINGS.get("NDS120HD"))

    ui_field_helper(header, "DRate", "600")

    index_mappings = {"kV": 0, "MV": 1, "Elec": 2}
    header.EnergyType.setCurrentIndex(index_mappings.get("MV"))
    ui_field_helper(header, "EnergyValue", "6")

    # okWidget = form.buttonBox.button(form.buttonBox.Ok)
    # header.getHeader()
    header.accept()

    def add_and_nav(form, fields):
        form.cpdata.add_cp(**dict(fields))

    form = header.cpWindow
    form.controlPoints = [form.controlPoints[0]]
    form.cpdata.cplist = [form.cpdata.cplist[0]]
    add_and_nav(form, [("Mu", "0"), ("GantryRtn", "0.00"),])
    add_and_nav(form, [("Mu", "40"), ("GantryRtn", "40.00"), ])
    add_and_nav(form, [("Mu", "100"), ("GantryRtn", "80.00"), ])
    add_and_nav(form, [("Mu", "140"), ("GantryRtn", "100.00"), ])
    add_and_nav(form, [("Mu", "170"), ("GantryRtn", "160.00"), ])
    add_and_nav(form, [("Mu", "180"), ("GantryRtn", "200.00"), ])

    form.editAddButton.click()  # click the editAdd button
    form.doneButton.click()

    #cp_xml_generate(form, NOSETEST_OUTPUT_PATH)
    image_points = []

    def add_image_points(point):
        image_points.append(point)

    point1 =  {'kvImageModeLabel': 'DynamicGain', 'kvAxisValue': '0', 'kvShape': '', 'kvdLng': '0', 'kvAxis': 'Mu', 'kvdPitch': '0', 'MilliAmperes': '40',
                'kvsLng': '0', 'kvAxisValueStop': '', 'kvsPitch': '0', 'kvsLat': '0', 'kvdVrt': '-70', 'kvdLat': '0', 'kvsVrt': '90', 'KiloVolts': '100', 'kvFoil': '', 'MilliSeconds': '10'}, None
    add_image_points(point1)

    point2 =  {'kvImageModeLabel': 'DynamicGainFluoro', 'kvAxisValue': '40', 'kvShape': '1', 'kvdLng': '', 'kvAxis': 'Mu', 'kvdPitch': '', 'MilliAmperes': '20',
                'kvsLng': '', 'kvAxisValueStop': '90', 'kvsPitch': '', 'kvsLat': '', 'kvdVrt': '', 'kvdLat': '', 'kvsVrt': '', 'KiloVolts': '100', 'kvFoil': '1', 'MilliSeconds': '20'}, None
    add_image_points(point2)

    cp_image = cpImage(None, IMAGE_INPUT_PATH, None)
    cp_image.image_only = False

    cpdata = form.cpdata
    cp_image.root = cpdata.create_xml()
    cp_image.sbeam = cp_image.root.getchildren()[0]

    # currently, this is broken, since previously it assumed
    # the tree was updated live, whereas now it's updated
    # after the cps have been filled out
    cp_image._set_image_params()
    for index, value in enumerate(image_points):
        kv_cp, mv_cp = value
        if kv_cp:
            cur_frac_cp = cp_image._generate_cp(kv_cp, "kvAxis", "kvAxisValue")
            cp_image.kvImagingNode(cp_image.root, kv_cp, cur_frac_cp, index)
        elif mv_cp:
            cur_frac_cp = cp_image._generate_cp(mv_cp, "mvAxis", "mvAxisValue")
            cp_image.mvImagingNode(cp_image.root, mv_cp, cur_frac_cp, index)

    validate_image_xml(cp_image.root)
    image_xml_generate(cp_image.root, NOSETEST_OUTPUT_PATH)
